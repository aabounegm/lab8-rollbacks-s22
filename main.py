import psycopg2

conn = psycopg2.connect("dbname=lab host=localhost password=admin user=admin")

buy_decrease_balance = r"""
    UPDATE Player
    SET balance = balance - (
            SELECT price
            FROM Shop
            WHERE product = %(product)s
        ) * %(amount)s
    WHERE username = %(username)s
"""
buy_decrease_stock = r"""
    UPDATE Shop
    SET in_stock = in_stock - %(amount)s
    WHERE product = %(product)s
"""
get_user_stock = r"""
    SELECT SUM(amount)
    FROM Inventory
    WHERE username = %(username)s
"""
buy_add_inventory = r"""
    UPDATE Inventory
    SET amount = amount + %(amount)s
    WHERE username = %(username)s AND product = %(product)s
"""


def buy_product(username, product, amount):
    with conn:
        with conn.cursor() as cur:
            obj = {"product": product, "username": username, "amount": amount}

            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Bad balance")

            try:
                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Product is out of stock")

            cur.execute(get_user_stock, obj)
            inventory_amount = cur.fetchone()[0]
            if inventory_amount + amount > 100:
                raise Exception("Inventory is full")

            cur.execute(buy_add_inventory, obj)

            conn.commit()
